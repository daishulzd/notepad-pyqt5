#notepad-pyqt5

> 这是仿照Windows系统自带记事本(Nodepad)应用写的一个基于Python_3.4.1 + PyQt_5.4.1的山寨记事本

## 历程
1. 前段时间在网上看到python挺火的，而且公司里有个同事python相当的牛逼，所以想学学
2. 作为Java程序狗，手上不会几门语言，都不敢混下去了
3. 上上次南京源创会，有位大神讲过Qt，所以想学学
4. 上家公司，我们都是使用markdown来写文档，但是markdown的编辑器markdownpad是收费的，免费版是阉割的，很不爽
5. 所以想自己写一个markdown编辑器
6. but，不想用Java写
7. so，选择了用python+qt
8. 所以呢，在写markdown之前，先自己写个记事本练练手
9. 昨晚同事请我吃饭回来后，大概8点不到这样，突发灵感，上手写
10. 刚开始，电脑上装的是python2.x+pyqt4，写起来有个问题一直没有解决掉，伤心的一B，索性换成python3.x+pyqt5了
11. 这个问题就是无法监听QtWidgets.QTextEdit中的文本发生变化的事件

## 使用到的技术
1. Python 3.x
2. Qt 5.x
3. PyQt 5.4.1
	- QtCore
	- QtGui
	- QtWidgets
	- QtPrintSupport


## 遇到的难点
1. 布局过于繁琐(下文会提到)
2. 截取系统事件
3. 对于QtWidgets.QTextEdit中文本的操作，如：选中事件、文本被修改事件等等
4. and so on

## 计划需要改进的
1. 将图片资源弄成qrc
2. 记事本加上文本内容检索功能
3. 在QtWidgets.QTextEdit编辑区替换原有的右键菜单
4. 更改主题
5. 使用Qt Designer设置ui图
6. 将配置文件保存在文件中
7. 打包成exe可安装文件

## changelog
1. 2015-04-21
	- 添加替换功能
	- 全部替换暂时未实现
	- 是否区分大小写替换未实现
2. 2015-04-20
	- 添加搜索功能，支持从光标位置处开始搜索
	- 是否区分大小写搜索未实现
3. 2015-04-19
	- 初始化基本功能

## 最后来一张效果图
![效果图](http://static.oschina.net/uploads/space/2015/0420/234511_dN2V_151849.png "效果图")